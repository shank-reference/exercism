use std::collections::HashMap;
use std::thread::{self, JoinHandle};

pub fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
    if input.is_empty() {
        return HashMap::new();
    }

    let worker_count = input.len().min(worker_count);
    let chunk_size = if input.len() % worker_count == 0 {
        input.len() / worker_count
    } else {
        input.len() / worker_count + 1
    };

    let handles: Vec<JoinHandle<_>> = input
        .chunks(chunk_size)
        .map(|chunk| chunk.iter().map(|s| s.to_owned().to_lowercase()).collect())
        .map(|chunk| thread::spawn(move || count_chars(chunk)))
        .collect();

    // fan-in the results from all worker threads
    let mut result = HashMap::new();
    handles.into_iter().for_each(|handle| {
        let res = handle.join().unwrap();
        res.iter().for_each(|(&ch, cnt)| {
            *result.entry(ch).or_default() += cnt;
        });
    });
    result
}

fn count_chars(input: Vec<String>) -> HashMap<char, usize> {
    let mut result = HashMap::new();
    input.into_iter().for_each(|w| {
        w.chars().filter(|ch| ch.is_alphabetic()).for_each(|ch| {
            *result.entry(ch).or_default() += 1;
        });
    });
    result
}
