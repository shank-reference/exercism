// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.

#[derive(PartialEq, Debug)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Debug)]
pub struct Robot {
    direction: Direction,
    location: (i32, i32),
}

impl Robot {
    pub fn new(x: i32, y: i32, d: Direction) -> Self {
        Self {
            direction: d,
            location: (x, y),
        }
    }

    pub fn turn_right(self) -> Self {
        let new_direction = match self.direction {
            Direction::North => Direction::East,
            Direction::East => Direction::South,
            Direction::West => Direction::North,
            Direction::South => Direction::West,
        };
        Self {
            direction: new_direction,
            ..self
        }
    }
    pub fn turn_left(self) -> Self {
        let new_direction = match self.direction {
            Direction::North => Direction::West,
            Direction::East => Direction::North,
            Direction::West => Direction::South,
            Direction::South => Direction::East,
        };
        Self {
            direction: new_direction,
            ..self
        }
    }

    pub fn advance(self) -> Self {
        let (dx, dy) = match self.direction {
            Direction::North => (0, 1),
            Direction::South => (0, -1),
            Direction::East => (1, 0),
            Direction::West => (-1, 0),
        };
        Self {
            location: (self.location.0 + dx, self.location.1 + dy),
            ..self
        }
    }

    pub fn instructions(self, instructions: &str) -> Self {
        instructions.chars().fold(self, |robot, inst| match inst {
            'A' => robot.advance(),
            'L' => robot.turn_left(),
            'R' => robot.turn_right(),
            _ => robot,
        })
    }

    pub fn position(&self) -> (i32, i32) {
        self.location
    }

    pub fn direction(&self) -> &Direction {
        &self.direction
    }
}
