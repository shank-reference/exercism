use std::io::{Read, Result, Write};

pub struct ReadStats<R>{
    inner: R,
    reads: usize,
    bytes_through: usize
}

impl<R: Read> ReadStats<R> {
    pub fn new(wrapped: R) -> ReadStats<R> {
        ReadStats{inner: wrapped, reads: 0, bytes_through: 0}
    }

    pub fn get_ref(&self) -> &R {
        &self.inner
    }

    pub fn bytes_through(&self) -> usize {
        self.bytes_through
    }

    pub fn reads(&self) -> usize {
        self.reads
    }
}

impl<R: Read> Read for ReadStats<R> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let res = self.inner.read(buf);
        match res {
            Ok(bytes) => {
                self.reads += 1;
                self.bytes_through += bytes;
                res
            },
            Err(e) => Err(e)
        }
    }
}

pub struct WriteStats<W>{
    inner: W,
    writes: usize,
    bytes_through: usize
}

impl<W: Write> WriteStats<W> {
    pub fn new(wrapped: W) -> WriteStats<W> {
        WriteStats{ inner: wrapped, writes: 0, bytes_through: 0}
    }

    pub fn get_ref(&self) -> &W {
        &self.inner
    }

    pub fn bytes_through(&self) -> usize {
        self.bytes_through
    }

    pub fn writes(&self) -> usize {
        self.writes
    }
}

impl<W: Write> Write for WriteStats<W> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let res = self.inner.write(buf);
        match res {
            Ok(bytes) => {
                self.bytes_through += bytes;
                self.writes += 1;
                res
            },
            Err(_) => res
        }
    }

    fn flush(&mut self) -> Result<()> {
        self.inner.flush()
    }
}
