/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    let chars: Vec<char> = code.chars().collect();

    let non_digit_char = chars
        .iter()
        .any(|&c| !(c.is_digit(10) || c.is_whitespace()));

    let one_or_zero_digit = chars.iter().filter(|&c| c.is_digit(10)).count() <= 1;

    if non_digit_char || one_or_zero_digit {
        return false;
    }

    let sum: u32 = chars
        .iter()
        .filter_map(|&c| c.to_digit(10))
        .rev()
        .enumerate()
        .map(|(i, d)| {
            if i % 2 == 0 {
                d
            } else {
                if d * 2 > 9 {
                    d * 2 - 9
                } else {
                    d * 2
                }
            }
        }).sum();

    sum % 10 == 0
}
