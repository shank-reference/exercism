use std::collections::HashSet;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    let lower_word = word.to_lowercase();
    let ord_word: Vec<char> = lowercase_ord_vec(word);

    possible_anagrams
        .iter()
        .filter(|candidate| candidate.to_lowercase() != lower_word)
        .filter(|candidate| ord_word == lowercase_ord_vec(candidate))
        .cloned()
        .collect()
}

fn lowercase_ord_vec(word: &str) -> Vec<char> {
    let mut res = word
        .chars()
        .flat_map(|c| c.to_lowercase())
        .collect::<Vec<char>>();

    res.sort();
    res
}
