pub mod graph {
    pub use super::Graph;

    pub mod graph_items {
        pub mod edge {
            pub use super::super::super::Edge;
        }

        pub mod node {
            pub use super::super::super::Node;
        }
    }
}

use std::collections::HashMap;
use std::convert::Into;

type Attrs = HashMap<String, String>;

pub struct Graph {
    pub nodes: Vec<Node>,
    pub edges: Vec<Edge>,
    pub attrs: Attrs,
}

impl Graph {
    pub fn new() -> Self {
        Self {
            nodes: vec![],
            edges: vec![],
            attrs: HashMap::new(),
        }
    }

    pub fn with_nodes(self, nodes: &[Node]) -> Self {
        Self {
            nodes: nodes.to_vec(),
            ..self
        }
    }

    pub fn with_edges(self, edges: &[Edge]) -> Self {
        Self {
            edges: edges.iter().cloned().collect(),
            ..self
        }
    }

    pub fn with_attrs(self, attrs: &[(&str, &str)]) -> Self {
        Self {
            attrs: attrs
                .iter()
                .map(|&(name, value)| (name.to_string(), value.to_string()))
                .collect(),
            ..self
        }
    }

    pub fn get_node(&self, name: &str) -> Option<Node> {
        self.nodes
            .iter()
            .find(|&node| node.elem == name)
            .map(|node| node.clone())
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct Edge {
    from: String,
    to: String,
    attrs: Attrs,
}

impl Edge {
    pub fn new<T1, T2>(from: T1, to: T2) -> Self
    where
        T1: Into<String>,
        T2: Into<String>,
    {
        Edge {
            from: from.into(),
            to: to.into(),
            attrs: HashMap::new(),
        }
    }

    pub fn with_attrs(self, attrs: &[(&str, &str)]) -> Self {
        Self {
            attrs: attrs
                .iter()
                .map(|&(name, value)| (name.to_string(), value.to_string()))
                .collect(),
            ..self
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Node {
    elem: String,
    attrs: Attrs,
}

impl Node {
    pub fn new<T>(elem: T) -> Self
    where
        T: Into<String>,
    {
        Self {
            elem: elem.into(),
            attrs: Attrs::default(),
        }
    }

    pub fn with_attrs(self, attrs: &[(&str, &str)]) -> Self {
        Self {
            attrs: attrs
                .iter()
                .map(|&(name, value)| (name.to_string(), value.to_string()))
                .collect(),
            ..self
        }
    }

    pub fn get_attr(&self, name: &str) -> Option<&str> {
        self.attrs.get(name).map(|value| value.as_ref())
    }
}
