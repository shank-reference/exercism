pub fn raindrops(n: u32) -> String {
    let words = [(3, "Pling"), (5, "Plang"), (7, "Plong")];

    let result: String = words
        .iter()
        .filter(|&(d, _)| n % d == 0)
        .map(|&(_, s)| s)
        .collect();

    if result.is_empty() {
        return n.to_string();
    } else {
        return result;
    }
}
