defmodule BankAccount do
  @typedoc """
  An account handle.
  """
  @opaque account :: pid

  @doc """
  Open the bank. Makes the account available.
  """
  @spec open_bank :: account
  def open_bank do
    {:ok, pid} = Agent.start(fn -> %{balance: 0, status: :active} end)
    pid
  end

  @doc """
  Close the bank. Makes the account unavailable.
  """
  @spec close_bank(account) :: none
  def close_bank(account) do
    Agent.update(account, fn state -> %{state | status: :closed} end)
  end

  @doc """
  Get the account's balance.
  """
  @spec balance(account) :: integer
  def balance(account) do
    Agent.get(account, fn
      %{status: :closed} -> {:error, :account_closed}
      %{status: :active, balance: balance} -> balance
    end)
  end

  @doc """
  Update the account's balance by adding the given amount which may be negative.
  """
  @spec update(account, integer) :: any
  def update(account, amount) do
    Agent.get_and_update(account, fn
      %{status: :closed} = state ->
        {{:error, :account_closed}, state}

      %{status: :active, balance: balance} = state ->
        {balance + amount, %{state | balance: balance + amount}}
    end)
  end
end
