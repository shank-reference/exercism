defmodule Queens do
  @type t :: %Queens{black: {integer, integer}, white: {integer, integer}}
  defstruct black: nil, white: nil

  @doc """
  Creates a new set of Queens
  """
  @spec new() :: Queens.t()
  @spec new({integer, integer}, {integer, integer}) :: Queens.t()

  def new(white \\ {0, 3}, black \\ {7, 3})

  def new(white, white), do: raise(ArgumentError)

  def new(white, black) do
    %Queens{black: black, white: white}
  end

  @doc """
  Gives a string representation of the board with
  white and black queen locations shown
  """
  @spec to_string(Queens.t()) :: String.t()
  def to_string(queens) do
    0..7
    |> Enum.map(&row(&1, queens.white, queens.black))
    |> Enum.join("\n")
  end

  @spec row(integer, {integer, integer}, {integer, integer}) :: String.t()
  def row(r, {wy, wx}, {by, bx}) do
    res = List.duplicate("_", 8)

    res =
      case r do
        ^wy -> List.replace_at(res, wx, "W")
        ^by -> List.replace_at(res, bx, "B")
        _r -> res
      end

    Enum.join(res, " ")
  end

  @doc """
  Checks if the queens can attack each other
  """
  @spec can_attack?(Queens.t()) :: boolean
  def can_attack?(%Queens{white: {wy, wx}, black: {by, bx}}) do
    wy == by or
      wx == bx or
      abs(wy - by) == abs(wx - bx)
  end
end
