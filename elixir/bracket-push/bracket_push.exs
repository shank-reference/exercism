defmodule BracketPush do
  @brackets ~w([ ] { } \( \))

  @doc """
  Checks that all the brackets and braces in the string are matched correctly, and nested correctly
  """
  @spec check_brackets(String.t()) :: boolean
  def check_brackets(""), do: true

  def check_brackets(str) do
    str
    |> String.graphemes()
    |> Enum.filter(&(&1 in @brackets))
    |> check
  end

  @spec check([String.t()], list()) :: boolean
  defp check(brackets, stack \\ [])
  defp check([], []), do: true
  defp check([], _), do: false
  defp check([b | rest], []), do: check(rest, [b])

  defp check([b | rest], [st_top | st_tail] = stack) do
    if st_top == opening(b) do
      check(rest, st_tail)
    else
      check(rest, [b | stack])
    end
  end

  defp opening("}"), do: "{"
  defp opening("]"), do: "["
  defp opening(")"), do: "("
  defp opening(_), do: nil
end
