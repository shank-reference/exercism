defmodule Series do
  @doc """
  Finds the largest product of a given number of consecutive numbers in a given string of numbers.
  """
  @spec largest_product(String.t(), non_neg_integer) :: non_neg_integer
  def largest_product("", 0), do: 1
  def largest_product("", _), do: raise(ArgumentError)
  def largest_product(_, size) when size < 0, do: raise(ArgumentError)

  def largest_product(number_string, size) when is_binary(number_string) do
    number_string |> to_digits |> largest_product(size)
  end

  @spec largest_product(list(integer), non_neg_integer) :: non_neg_integer
  def largest_product(digits, size) when is_list(digits) and length(digits) < size,
    do: raise(ArgumentError)

  def largest_product(digits, size) when is_list(digits) do
    0..(length(digits) - size)
    |> Enum.reduce(0, &max(product(digits, &1, size), &2))
  end

  @spec to_digits(String.t()) :: list(integer)
  defp to_digits(number_string) do
    number_string
    |> String.split("", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  @spec product(list(integer), integer, integer) :: integer
  defp product([0], _, _), do: 0

  defp product(xs, start_idx, size) do
    xs
    |> Enum.drop(start_idx)
    |> Enum.take(size)
    |> Enum.reduce(1, &(&1 * &2))
  end
end
