defmodule Raindrops do
  @factor_output %{
    3 => "Pling",
    5 => "Plang",
    7 => "Plong"
  }

  @doc """
  Returns a string based on raindrop factors.

  - If the number contains 3 as a prime factor, output 'Pling'.
  - If the number contains 5 as a prime factor, output 'Plang'.
  - If the number contains 7 as a prime factor, output 'Plong'.
  - If the number does not contain 3, 5, or 7 as a prime factor,
    just pass the number's digits straight through.
  """
  @spec convert(pos_integer) :: String.t
  def convert(number) do
    convert(number, Map.keys(@factor_output), [])
  end

  def convert(number, [], []    ), do: to_string(number)
  def convert(_     , [], parts ), do: parts |> Enum.reverse |> Enum.join
  def convert(number, [h|t], parts) do
    if rem(number, h) == 0 do
      convert(number, t, [@factor_output[h] | parts])
    else
      convert(number, t, parts)
    end
  end

end
