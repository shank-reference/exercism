defmodule RunLengthEncoder do
  @doc """
  Generates a string where consecutive elements are represented as a data value and count.
  "AABBBCCCC" => "2A3B4C"
  For this example, assume all input are strings, that are all uppercase letters.
  It should also be able to reconstruct the data into its original form.
  "2A3B4C" => "AABBBCCCC"
  """
  @spec encode(String.t) :: String.t
  def encode(string) do
    string
    |> String.graphemes
    |> Enum.chunk_by(&(&1))
    |> Enum.reduce("", fn(x, acc) ->
      case length(x) do
        1 -> acc <> List.first(x)
        i -> acc <> Integer.to_string(i) <> List.first(x)
      end
    end)
  end

  @spec decode(String.t) :: String.t
  def decode(string) do
    Regex.replace(~r/(\d+)([\p{L} ])/, string, fn _m, count, ch ->
      String.duplicate(ch, String.to_integer(count))
    end)
  end

end
