defmodule Markdown do
  @doc """
    Parses a given string with Markdown syntax and returns the associated HTML for that string.

    ## Examples

    iex> Markdown.parse("This is a paragraph")
    "<p>This is a paragraph</p>"

    iex> Markdown.parse("#Header!\n* __Bold Item__\n* _Italic Item_")
    "<h1>Header!</h1><ul><li><em>Bold Item</em></li><li><i>Italic Item</i></li></ul>"
  """
  @spec parse(String.t()) :: String.t()
  def parse(markdown_str) do
    markdown_str
    |> String.split("\n")
    |> Enum.map_join(&process/1)
    |> put_list_items_in_ul()
  end

  defp process(<<"*", _rest::binary>> = line), do: parse_list_item(line)

  defp process(<<"#", _rest::binary>> = line) do
    line |> parse_header |> enclose_with_header_tag
  end

  defp process(line), do: line |> String.split() |> enclose_with_paragraph_tag

  defp parse_header(header) do
    [h | t] = String.split(header)
    {to_string(String.length(h)), Enum.join(t, " ")}
  end

  defp parse_list_item(list_str) do
    list_item = list_str |> String.trim_leading("* ") |> String.split()
    "<li>#{join_words_with_tags(list_item)}</li>"
  end

  defp enclose_with_header_tag({level, content}) do
    "<h#{level}>#{content}</h#{level}>"
  end

  defp enclose_with_paragraph_tag(content) do
    "<p>#{join_words_with_tags(content)}</p>"
  end

  defp join_words_with_tags(parts) do
    Enum.map_join(parts, " ", &replace_md_with_tag/1)
  end

  defp replace_md_with_tag(w) do
    w |> replace_prefix_md |> replace_suffix_md
  end

  defp replace_prefix_md(w) do
    cond do
      w =~ ~r/^#{"__"}{1}/ -> String.replace(w, ~r/^#{"__"}{1}/, "<strong>", global: false)
      w =~ ~r/^[#{"_"}{1}][^#{"_"}+]/ -> String.replace(w, ~r/_/, "<em>", global: false)
      true -> w
    end
  end

  defp replace_suffix_md(w) do
    cond do
      w =~ ~r/#{"__"}{1}$/ -> String.replace(w, ~r/#{"__"}{1}$/, "</strong>")
      w =~ ~r/[^#{"_"}{1}]/ -> String.replace(w, ~r/_/, "</em>")
      true -> w
    end
  end

  defp put_list_items_in_ul(parsed) do
    String.replace_suffix(
      String.replace(parsed, "<li>", "<ul><li>", global: false),
      "</li>",
      "</li></ul>"
    )
  end
end
