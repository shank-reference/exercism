defmodule Pangram do
  @doc """
  Determines if a word or sentence is a pangram.
  A pangram is a sentence using every letter of the alphabet at least once.

  Returns a boolean.

    ## Examples

      iex> Pangram.pangram?("the quick brown fox jumps over the lazy dog")
      true

  """

  @spec pangram?(String.t) :: boolean
  def pangram?(sentence) do
    lcase_sentence = 
      sentence
      |> String.replace("_", "")
      |> String.replace(" ", "")
      |> String.downcase
    
    ?a..?z
    |> Enum.map(&(<<&1::utf8>>))
    |> Enum.all?(&String.contains?(lcase_sentence, &1))
  end
end
