defmodule Sublist do
  @doc """
  Returns whether the first list is a sublist or a superlist of the second list
  and if not whether it is equal or unequal to the second list.
  """
  def compare(a, b) do
    cond do
      a === b                                   -> :equal
      length(a) < length(b) && contains?(a, b)  -> :sublist
      length(a) > length(b) && contains?(b, a)  -> :superlist
      true                                      -> :unequal
    end
  end

  def contains?([], _b), do: true
  def contains?(a, b) do
    Stream.chunk(b, length(a), 1) |> Enum.any?(&(&1 === a))
  end

end
